import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Racer"]


class Racer(Base):
	"""A racer."""

	__tablename__ = "racers"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	number = sqlalchemy.Column(
		sqlalchemy.Integer,
		nullable=False
	)

	name = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)

	time = sqlalchemy.Column(
		sqlalchemy.Interval,
		nullable=False
	)
