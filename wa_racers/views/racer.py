from __future__ import annotations

import datetime
import hashlib
import http.client
import io
import typing
import uuid

import flask
import flask_babel
import flask_wtf
import sqlalchemy
import werkzeug.exceptions
import wtforms
import wtforms.validators

from .. import database

__all__ = [
	"RacerCreationEditForm",
	"add",
	"delete",
	"edit",
	"list_",
	"view",
	"racer_blueprint"
]


racer_blueprint = flask.Blueprint(
	"racer",
	__name__,
	url_prefix="/racers"
)


class RacerCreationEditForm(flask_wtf.FlaskForm):
	number = wtforms.fields.IntegerField(
		flask_babel.lazy_gettext("Number"),
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.NumberRange(min=1, max=2**31)
		]
	)

	name = wtforms.fields.StringField(
		flask_babel.lazy_gettext("Name"),
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(min=1, max=128)
		]
	)

	time = wtforms.fields.IntegerField(
		flask_babel.lazy_gettext("Amount of seconds"),
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.NumberRange(min=1, max=2**31)
		]
	)


@racer_blueprint.route("/")
def list_() -> typing.Tuple[flask.Response, int]:
	order_by = flask.request.args.get("order-by", "number")
	order_type = flask.request.args.get("order-type", "desc")

	if (
		order_by not in ("number", "name", "time") or
		order_type not in ("asc", "desc")
	):
		raise werkzeug.exceptions.BadRequest

	racers = flask.g.sa_session.execute(
		sqlalchemy.select(database.Racer).
		order_by(
			getattr(sqlalchemy, order_type)(
				getattr(database.Racer, order_by)
			)
		)
	).scalars().all()

	return flask.render_template(
		"racer_list.html",
		racers=racers,
		order_by=order_by,
		order_type=order_type
	), http.client.OK


@racer_blueprint.route("/<uuid:id_>")
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	racer = flask.g.sa_session.get(
		database.Racer,
		id_
	)

	if racer is None:
		raise werkzeug.exceptions.NotFound

	return flask.render_template(
		"racer_view.html",
		racer=racer
	), http.client.OK


@racer_blueprint.route("/add", methods=["GET", "POST"])
def add()  -> typing.Tuple[flask.Response, int]:
	form = RacerCreationEditForm()
	
	if form.validate_on_submit():
		flask.g.sa_session.add(
			database.Racer(
				number=form.number.data,
				name=form.name.data,
				time=datetime.timedelta(seconds=form.time.data)
			)
		)

		flask.g.sa_session.commit()

		return flask.redirect(flask.url_for("racer.list_"))

	return flask.render_template(
		"racer_add.html",
		form=form
	), http.client.OK


@racer_blueprint.route("/delete/<uuid:id_>")
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	racer = flask.g.sa_session.get(
		database.Racer,
		id_
	)

	if racer is None:
		raise werkzeug.exceptions.NotFound

	flask.g.sa_session.delete(racer)
	flask.g.sa_session.commit()

	return flask.redirect(flask.url_for("racer.list_"))


@racer_blueprint.route("/edit/<uuid:id_>", methods=["GET", "POST"])
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	racer = flask.g.sa_session.get(
		database.Racer,
		id_
	)

	if racer is None:
		raise werkzeug.exceptions.NotFound

	form = RacerCreationEditForm()
	
	if form.validate_on_submit():
		racer.number = form.number.data
		racer.name = form.name.data
		racer.time = datetime.timedelta(seconds=form.time.data)

		return flask.redirect(flask.url_for("racer.view", id_=racer.id))

	return flask.render_template(
		"racer_edit.html",
		racer=racer,
		form=form
	), http.client.OK
